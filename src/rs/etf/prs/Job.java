package rs.etf.prs;

/**
 * User: Krca
 * Date: 9/2/12
 * Time: 10:58 AM
 */
public class Job {
    private static int totalId = 0;
    private int id = totalId++;
    private double spentInProc = 0;
    private double totalTime = 0;
    private double started = 0;
    private double endTime = 0;
    private int numOfTimesInProcessor = 0;

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public double getStarted() {
        return started;
    }

    public void setStarted(double started) {
        this.started = started;
    }

    public int getId() {
        return id;
    }

    public double getSpentInProc() {
        return spentInProc;
    }

    public void setSpentInProc(double spentInProc) {
        this.spentInProc = spentInProc;
    }

    public void incSpentInProc(double delta) {
        this.spentInProc += delta;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }

    public void incTotalTimeBy(double delta) {
        this.totalTime += delta;
    }

    public void incNumOfTimesInProcessor() {
        this.numOfTimesInProcessor++;
    }

    public int getNumOfTimesInProcessor() {
        return numOfTimesInProcessor;
    }
}
