package rs.etf.prs;

/**
 * User: Milos Krsmanovic
 * Date: 9/2/12
 * Time: 11:05 AM
 */
public class Link {

    private Resource resource = null;
    private double probability = 0;

    public Link(double probability, Resource resource) {
        this.resource = resource;
        this.probability = probability;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void addJobToResource(Job job) {
        this.resource.addJob(job);
    }
}
