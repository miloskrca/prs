package rs.etf.prs;

import java.io.*;
import java.util.Date;

/**
 * User: Krca
 * Date: 9/2/12
 * Time: 4:24 PM
 */

public class PerformanceSystem {

    public static void startSystem(int jobNum, int discNum) {

        Resource processor = new Resource(5);
        processor.setProcessor();
        Resource sDisc1 = new Resource(12);
        Resource sDisc2 = new Resource(15);

        Resource[] uDisks = new Resource[discNum];
        Resource[] allResources = new Resource[3 + uDisks.length];
        Link[] processorLinkList = new Link[2 + uDisks.length];
        Link[] sDisk1LinkList = new Link[1 + uDisks.length];
        Link[] sDisk2LinkList = new Link[1 + uDisks.length];

        allResources[0] = processor;
        allResources[1] = sDisc1;
        allResources[2] = sDisc2;

        processorLinkList[0] = new Link(0.15, sDisc1);
        processorLinkList[1] = new Link(0.15, sDisc2);
        sDisk1LinkList[0] = new Link(0.5, processor);
        sDisk2LinkList[0] = new Link(0.5, processor);

        for (int i = 0; i < discNum; i++) {
            uDisks[i] = new Resource(20);
            uDisks[i].setLinkList(new Link[]{new Link(1, processor)});
            allResources[i + 3] = uDisks[i];
            processorLinkList[i + 2] = new Link(0.7 / discNum, uDisks[i]);
            sDisk1LinkList[i + 1] = new Link(0.5 / discNum, uDisks[i]);
            sDisk2LinkList[i + 1] = new Link(0.5 / discNum, uDisks[i]);
        }

        processor.setLinkList(processorLinkList);

        sDisc1.setLinkList(sDisk1LinkList);
        sDisc2.setLinkList(sDisk2LinkList);

        Job[] jobs = new Job[jobNum];
        for (int i = 0; i < jobNum; i++) {
            jobs[i] = new Job();
            processor.addJobToJobList(jobs[i]);
        }

        long start = (new Date()).getTime();

        Utils.resetNow();
        processor.loadNextJob();
        while (Utils.getNow() < Utils._18h) {

            Resource r = Utils.getResourceWithMinEndTime(allResources);
            Job j = r.getCurrentJob();
            // setNow
            Utils.setNow(j.getEndTime());
            // addJob
            Utils.calc(r.getLinkList()).getResource().addJob(j);
            // loadJob
            r.loadNextJob();

        }

        try {
            // Create file
            FileWriter fstream = new FileWriter("simulation.txt");
            BufferedWriter simFile = new BufferedWriter(fstream);

            simFile.write("Simulacija: n = " + jobNum + ", k = " + discNum + "\n\n");
            simFile.write("Iskoriscenje procesora: " + processor.getWorkingTime() / Utils._18h + "\n");
            simFile.write("Iskoriscenje sis.diska 1: " + sDisc1.getWorkingTime() / Utils._18h + "\n");
            simFile.write("Iskoriscenje sis.diska 2: " + sDisc2.getWorkingTime() / Utils._18h + "\n");
            for (int i = 0; i < discNum; i++)
                simFile.write("Iskoriscenje kor.diska " + i + ": " + uDisks[i].getWorkingTime() / Utils._18h + "\n");

            simFile.write("\nProtok kroz procesor: " + (processor.getTotalJobs() * 1000 / Utils._18h) + " poslova/s" + "\n");
            simFile.write("Protok kroz sis.disk 1: " + (sDisc1.getTotalJobs() * 1000 / Utils._18h) + " poslova/s" + "\n");
            simFile.write("Protok kroz sis.disk 2: " + (sDisc2.getTotalJobs() * 1000 / Utils._18h) + " poslova/s" + "\n");
            for (int i = 0; i < discNum; i++)
                simFile.write("Protok kroz kor.disk " + i + ": " + (uDisks[i].getTotalJobs() * 1000 / Utils._18h) + " poslova/s" + "\n");
            simFile.write("\n");

            simFile.write("Prosecan broj poslova u procesoru: " + (processor.averageTimeInResource() / Utils._18h) + "\n");
            simFile.write("Prosecan broj poslova u sis.disku 1: " + (sDisc1.averageTimeInResource() / Utils._18h) + "\n");
            simFile.write("Prosecan broj poslova u sis.disku 2: " + (sDisc2.averageTimeInResource() / Utils._18h) + "\n");
            for (int i = 0; i < discNum; i++)
                simFile.write("Prosecan broj poslova u kor.disku " + i + ": " + (uDisks[i].averageTimeInResource() / Utils._18h) + "\n");
            simFile.write("\n");

            double cycleTime = 0;
            for (int i = 0; i < jobNum; i++) cycleTime += jobs[i].getNumOfTimesInProcessor();
            simFile.write("Vreme odziva sistema je " + Utils._18h / (cycleTime / jobNum) / 1000 + "\n\n");

            simFile.write("\nSimulation lasted: " + ((new Date()).getTime() - start) + "ms");

            simFile.close();

            // Create file
            FileWriter fstreama = new FileWriter("analysis.txt");
            BufferedWriter analysisFile = new BufferedWriter(fstreama);

            analysisFile.write("Analiza: n = " + jobNum + ", k = " + discNum + "\n\n");

            /*
                   Buzen
             */
            double[] x = new double[discNum + 3];
            x[0] = 1;
            x[1] = 0.36;
            x[2] = 0.45;
            for (int i = 0; i < discNum; i++)
                x[i + 3] = 3.4 / discNum;

            double G[] = new double[jobNum + 1];
            G[0] = 1;
            for (int i = 1; i <= jobNum; i++) G[i] = 0;
            for (int j = 0; j < discNum + 3; j++)
                for (int i = 1; i <= jobNum; i++)
                    G[i] += x[j] * G[i - 1];

            double jP = 0, jD1 = 0, jD2 = 0, jDk = 0;
            for (int i = 1; i <= jobNum; i++) {
                jP += Math.pow(x[0], i) * (G[jobNum - i] / G[jobNum]);
                jD1 += Math.pow(x[1], i) * (G[jobNum - i] / G[jobNum]);
                jD2 += Math.pow(x[2], i) * (G[jobNum - i] / G[jobNum]);
                jDk += Math.pow(x[3], i) * (G[jobNum - i] / G[jobNum]);
            }
            //******************************************************************************************

            analysisFile.write("Iskoriscenje procesora: " + x[0] * G[jobNum - 1] / G[jobNum] + "\n");
            analysisFile.write("Iskoriscenje sis.diska 1: " + x[1] * G[jobNum - 1] / G[jobNum] + "\n");
            analysisFile.write("Iskoriscenje sis.diska 2: " + x[2] * G[jobNum - 1] / G[jobNum] + "\n");
            for (int i = 0; i < discNum; i++)
                analysisFile.write("Iskoriscenje kor.diska " + i + ": " + x[3] * G[jobNum - 1] / G[jobNum] + "\n");
            analysisFile.write("\n");
            analysisFile.write("Protok kroz procesor: " + 1000 * (x[0] * G[jobNum - 1] / G[jobNum]) / processor.s() + " poslova/s" + "\n");
            analysisFile.write("Protok kroz sis.disk 1: " + 1000 * (x[1] * G[jobNum - 1] / G[jobNum]) / sDisc1.s() + " poslova/s" + "\n");
            analysisFile.write("Protok kroz sis.disk 2: " + 1000 * (x[2] * G[jobNum - 1] / G[jobNum]) / sDisc2.s() + " poslova/s" + "\n");
            for (int i = 0; i < discNum; i++)
                analysisFile.write("Protok kroz kor.disk " + i + ": " + 1000 * (x[3] * G[jobNum - 1] / G[jobNum]) / uDisks[0].s() + " poslova/s" + "\n");
            analysisFile.write("\n");

            analysisFile.write("Prosecan broj poslova u procesoru: " + jP + "\n");
            analysisFile.write("Prosecan broj poslova u sis.disku 1: " + jD1 + "\n");
            analysisFile.write("Prosecan broj poslova u sis.disku 2: " + jD2 + "\n");
            for (int i = 0; i < discNum; i++)
                analysisFile.write("Prosecan broj poslova u kor.disku " + i + ": " + jDk + "\n");
            analysisFile.write("\n");
            analysisFile.write("Vreme odziva sistema je " + jobNum / (1000 * (x[0] * G[jobNum - 1] / G[jobNum]) / processor.s()) + "\n");
            analysisFile.write("\n");

            analysisFile.close();

            // Create file
            FileWriter fstreamt = new FileWriter("relative.txt");
            BufferedWriter rel = new BufferedWriter(fstreamt);

            rel.write("Relativna odstupanja za n=" + jobNum + ", k=" + discNum + "\n\n");

            rel.write("Iskoriscenje procesora: " + Utils.relative(processor.getWorkingTime() / Utils._18h, x[0] * G[jobNum - 1] / G[jobNum]) + " %\n");
            rel.write("Iskoriscenje sis.diska 1: " + Utils.relative(sDisc1.getWorkingTime() / Utils._18h, x[1] * G[jobNum - 1] / G[jobNum]) + " %\n");
            rel.write("Iskoriscenje sis.diska 2: " + Utils.relative(sDisc2.getWorkingTime() / Utils._18h, x[2] * G[jobNum - 1] / G[jobNum]) + " %\n");
            for (int i = 0; i < discNum; i++)
                rel.write("Iskoriscenje kor.diska " + i + ": " + Utils.relative(uDisks[i].getWorkingTime() / Utils._18h, x[3] * G[jobNum - 1] / G[jobNum]) + " %\n");
            rel.write("\n");
            rel.write("Protok kroz procesor: " + Utils.relative((processor.getTotalJobs() / Utils._18h) * 1000, 1000 * (x[0] * G[jobNum - 1] / G[jobNum]) / processor.s()) + " %\n");
            rel.write("Protok kroz sis.disk 1: " + Utils.relative((sDisc1.getTotalJobs() / Utils._18h) * 1000, 1000 * (x[1] * G[jobNum - 1] / G[jobNum]) / sDisc1.s()) + " %\n");
            rel.write("Protok kroz sis.disk 2: " + Utils.relative((sDisc2.getTotalJobs() / Utils._18h) * 1000, 1000 * (x[2] * G[jobNum - 1] / G[jobNum]) / sDisc2.s()) + " %\n");
            for (int i = 0; i < discNum; i++)
                rel.write("Protok kroz kor.disk " + i + ": " + Utils.relative((uDisks[i].getTotalJobs() / Utils._18h) * 1000, 1000 * (x[3] * G[jobNum - 1] / G[jobNum]) / uDisks[0].s()) + " %\n");
            rel.write("\n");

            rel.write("Prosecan broj poslova u procesoru: " + Utils.relative((processor.averageTimeInResource() / Utils._18h), jP) + " %\n");
            rel.write("Prosecan broj poslova u sis.disku 1: " + Utils.relative((sDisc1.averageTimeInResource() / Utils._18h), jD1) + " %\n");
            rel.write("Prosecan broj poslova u sis.disku 2: " + Utils.relative((sDisc2.averageTimeInResource() / Utils._18h), jD2) + " %\n");
            for (int i = 0; i < discNum; i++)
                rel.write("Prosecan broj poslova u kor.disku " + i + ": " + Utils.relative(uDisks[i].averageTimeInResource() / Utils._18h, jDk) + " %\n");
            rel.write("\n");
            rel.write("Vreme odziva sistema je " + Utils.relative(Utils._18h / (cycleTime / jobNum) / 1000, jobNum / (1000 * (x[0] * G[jobNum - 1] / G[jobNum]) / processor.s())) + " %\n\n");

            rel.close();


        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

}
