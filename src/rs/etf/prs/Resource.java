package rs.etf.prs;

import java.util.LinkedList;

/**
 * User: Krca
 * Date: 9/2/12
 * Time: 11:05 AM
 */
public class Resource {

    private Job currentJob = null;
    private Link[] linkList = null;
    private double averageWorkTime = 0;
    private LinkedList<Job> jobList = new LinkedList<Job>();
    private boolean isProcessor = false;

    private double workingTime = 0;
    private long totalJobs = 0;

    public Resource(double averageWorkTime, Link[] linkList) {
        this.averageWorkTime = averageWorkTime;
        this.linkList = linkList;
    }

    public Resource(double averageWorkTime) {
        this.averageWorkTime = averageWorkTime;
    }

    public void setProcessor() {
        isProcessor = true;
    }

    public void addJobToJobList(Job job) {
        jobList.add(job);
    }

    public void setJobList(LinkedList<Job> jobList) {
        this.jobList = jobList;
    }

    public Link[] getLinkList() {
        return linkList;
    }

    public void setLinkList(Link[] linkList) {
        this.linkList = linkList;
    }

    public void addJob(Job job) {
        if (jobList.size() == 0 && currentJob == null) {
            currentJob = job;
            prepareJob(job);
        } else {
            jobList.add(job);
        }
    }

    private void prepareJob(Job job) {
        job.setStarted(Utils.getNow());
        double workTime = this.calcTime();
        job.setEndTime(Utils.getNow() + workTime);

        if (this.isProcessor) {
            job.incNumOfTimesInProcessor();
        }
        this.workingTime += workTime;
        this.totalJobs++;
    }

    public void loadNextJob() {
        currentJob = (jobList.size() > 0 ? jobList.removeFirst() : null);
        if (currentJob != null) {
            prepareJob(currentJob);
        }
    }

    public double getEndTimeForCurrentJob() {
        if (this.currentJob != null) {
            return this.currentJob.getEndTime();
        }
        return -1;
    }

    private double calcTime() {
        return -this.averageWorkTime * Math.log(Math.random());
    }

    public Job getCurrentJob() {
        return currentJob;
    }

    public LinkedList<Job> getJobList() {
        return jobList;
    }

    public double getWorkingTime() {
        return workingTime;
    }

    public long getTotalJobs() {
        return totalJobs;
    }

    public double s() {
        return this.averageWorkTime;
    }

    public double averageTimeInResource() {
        return totalJobs / workingTime;
    }

    public String toString() {
        String s = "[";

        for (int i = 0; i < jobList.size(); i++) {
            s += " " + jobList.get(i).getId();
        }

        s += "] curr: " + (currentJob != null ? currentJob.getId() : "null");
        s += " endTime: " + (currentJob != null ? currentJob.getEndTime() : "null");

        return s;
    }

}
