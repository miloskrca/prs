package rs.etf.prs;

/**
 * User: Krca
 * Date: 9/2/12
 * Time: 12:01 PM
 */
public abstract class Utils {
    public static double now = 0;
    public static final double _18h = 18 * 60 * 60 * 1000;

    public static void incNowBy(double delta) {
        Utils.now += delta;
    }

    public static double getNow() {
        return Utils.now;
    }

    public static void resetNow() {
        Utils.now = 0;
    }

    public static void setNow(double now) {
        Utils.now = now;
    }

    public static Resource getResourceWithMinEndTime(Resource[] resources) {

        Resource min = null;

        for (int i = 0; i < resources.length; i++) {
            if (resources[i].getCurrentJob() != null) {
                min = resources[i];
                break;
            }
        }
            for (int i = 0; i < resources.length; i++) {
                if (resources[i].getCurrentJob() == null) continue;
                if (resources[i].getEndTimeForCurrentJob() < min.getEndTimeForCurrentJob()) {
                    min = resources[i];
                }
            }

        return min;
    }

    public static Link calc(Link[] links) {
        double rand = Math.random() * 100;
        double [] scaled = new double[links.length];
        double total = 0;
        for (int i = 0; i < scaled.length; i++) {
            total += links[i].getProbability() * 100;
            scaled[i] = total;
        }

        for (int i = 0; i < scaled.length; i++) {
            if (rand < scaled[i]) {
                return links[i];
            }
        }
        return null;
    }

    public static double relative(double a, double b) {
        double result;
        result = ((a - b) / a) * 100;
        if (result < 0) return -result;
        return result;
    }
}